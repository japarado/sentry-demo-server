use std::path::{Path, PathBuf};

use anyhow::Result;
use config::Config;
use secrecy::Secret;
use serde::Deserialize;
use thiserror::Error;

pub use application::ApplicationSettings;
pub use database::DatabaseSettings;
pub use environment::Environment;

mod application;
mod database;
mod environment;

#[derive(Error, Debug)]
pub enum SettingError
{
    #[error("Invalid environment. Found: {found:?}. Available values: {available:?}")]
    InvalidEnvironment
    {
        found: String,
        available: Vec<Environment>,
    },
}

#[derive(Deserialize, Clone, Debug)]
pub struct Settings
{
    pub application: ApplicationSettings,
    pub database: DatabaseSettings,
    pub logging: LoggingSettings,
    pub integration_test: IntegrationTestSettings,
    pub redis_uri: Secret<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct LoggingSettings
{
    pub level: String,
}

#[derive(Deserialize, Clone, Debug)]
pub struct IntegrationTestSettings
{
    pub use_ephemeral_db: bool,
}

impl Settings
{
    pub fn assemble() -> Result<Settings, config::ConfigError>
    {
        let base_path = Self::workspace_dir();
        let configuration_directory = base_path.join("configuration");
        let environment: Environment = std::env::var("APP_ENVIRONMENT")
            .unwrap_or_else(|_| "local".into())
            .try_into()
            .expect("Failed to parse APP_ENVIRONMENT");
        let settings = Config::builder()
            .add_source(config::File::from(configuration_directory.join("base")))
            .add_source(
                config::File::from(configuration_directory.join(environment.as_str()))
                    .required(true),
            )
            .add_source(config::Environment::with_prefix("app").separator("_"))
            .build()?;
        settings.try_deserialize()
    }

    // TODO: Better error handling
    fn workspace_dir() -> PathBuf
    {
        let output = std::process::Command::new(env!("CARGO"))
            .arg("locate-project")
            .arg("--workspace")
            .arg("--message-format=plain")
            .output()
            .unwrap()
            .stdout;
        let cargo_path = Path::new(std::str::from_utf8(&output).unwrap().trim());
        cargo_path.parent().unwrap().to_path_buf()
    }
}
