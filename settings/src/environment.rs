use crate::SettingError;
use crate::SettingError::InvalidEnvironment;

#[derive(Debug)]
pub enum Environment
{
    Local,
    Production,
}

impl Environment
{
    pub fn as_str(&self) -> &'static str
    {
        match self
        {
            Environment::Local => "local",
            Environment::Production => "production",
        }
    }
}

impl TryFrom<String> for Environment
{
    type Error = SettingError;

    fn try_from(s: String) -> Result<Self, Self::Error>
    {
        match s.to_lowercase().as_str()
        {
            "local" => Ok(Self::Local),
            "production" => Ok(Self::Production),
            other => Err(InvalidEnvironment {
                found: other.to_string(),
                available: vec![Environment::Local, Environment::Production],
            }),
        }
    }
}

#[cfg(test)]
mod tests
{
    #[test]
    #[ignore = "Unimplemented"]
    fn local_env_should_work()
    {
        todo!()
    }

    #[test]
    #[ignore = "Unimplemented"]
    fn production_env_should_work()
    {
        todo!()
    }

    #[test]
    #[ignore = "Unimplemented"]
    fn invalid_env_should_throw_error()
    {
        todo!()
    }
}
