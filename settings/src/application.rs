use secrecy::Secret;
use serde::Deserialize;

#[derive(Deserialize, Clone, Debug)]
pub struct ApplicationSettings
{
    pub port: u16,
    pub host: String,
    pub base_url: String,
    pub hmac_secret: Secret<String>,
}
