use std::fmt::{Display, Formatter};

use sea_orm::ConnectOptions;
use secrecy::{ExposeSecret, Secret};
use serde::Deserialize;

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum DbType
{
    Postgres,
    Mysql,
    Sqlite,
}

impl Display for DbType
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        let value = match self
        {
            DbType::Postgres => "postgres".to_string(),
            DbType::Mysql => "mysql".to_string(),
            DbType::Sqlite => "sqlite".to_string(),
        };
        write!(f, "{}", value)
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct DatabaseSettings
{
    pub username: String,
    pub password: Secret<String>,
    pub port: u16,
    pub host: String,
    pub db: String,
    pub max_connections: u32,
    pub min_connections: u32,
    pub db_type: DbType,
}

impl DatabaseSettings
{
    pub fn with_db(&self) -> ConnectOptions
    {
        ConnectOptions::new(self.generate_connection_string())
            .max_connections(self.max_connections)
            .min_connections(self.min_connections)
            .to_owned()
    }

    // TODO: Deprecate in favor of ConnectionOptions::get_url()
    fn generate_connection_string(&self) -> String
    {
        format!(
            "{}://{}:{}@{}/{}",
            self.db_type,
            self.username,
            self.password.expose_secret(),
            self.host,
            self.db
        )
    }
}

impl Display for DatabaseSettings
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        write!(
            f,
            "{}://{}:{}@{}/{}",
            self.db_type,
            self.username,
            self.password.expose_secret(),
            self.host,
            self.db,
        )
    }
}

#[cfg(test)]
mod tests
{
    use secrecy::Secret;

    use DbType::Postgres;

    use crate::database::DbType;

    use super::DatabaseSettings;

    #[test]
    fn test_create_db_connection_settings()
    {
        let db_settings = DatabaseSettings {
            db: "test-database".to_string(),
            db_type: Postgres,
            host: "localhost".to_string(),
            max_connections: 24,
            min_connections: 24,
            password: Secret::new("password".to_string()),
            port: 5432,
            username: "username".to_string(),
        };

        let connection_options = db_settings.with_db();

        assert_eq!(connection_options.get_max_connections(), Some(24));
        assert_eq!(connection_options.get_min_connections(), Some(24));
        assert_eq!(
            connection_options.get_url(),
            "postgres://username:password@localhost/test-database"
        )
    }
}
