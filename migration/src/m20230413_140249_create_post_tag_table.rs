use sea_orm_migration::prelude::*;

use entity::{post, post_tag, tag};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .create_table(
                Table::create()
                    .table(PostTag::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(PostTag::PostId).integer().not_null())
                    .col(ColumnDef::new(PostTag::TagId).integer().not_null())
                    .foreign_key(
                        ForeignKeyCreateStatement::new()
                            .from(post_tag::Entity, post_tag::Column::PostId)
                            .to(post::Entity, post::Column::Id),
                    )
                    .foreign_key(
                        ForeignKeyCreateStatement::new()
                            .from(post_tag::Entity, post_tag::Column::TagId)
                            .to(tag::Entity, tag::Column::Id),
                    )
                    .primary_key(
                        IndexCreateStatement::new()
                            .col(PostTag::TagId)
                            .col(PostTag::PostId),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .drop_table(Table::drop().table(PostTag::Table).to_owned())
            .await
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
enum PostTag
{
    Table,
    PostId,
    TagId,
}
