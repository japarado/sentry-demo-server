use rand::prelude::SliceRandom;
use rand::Rng;
use sea_orm_migration::prelude::*;

use entity::tag::Model;
use entity::{post, post_tag, tag};

use crate::sea_orm::ActiveValue::Set;
use crate::sea_orm::EntityTrait;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        let db = manager.get_connection();

        let posts = post::Entity::find().all(db).await?;
        let tags = tag::Entity::find().all(db).await?;

        let mut rng = rand::rngs::OsRng;

        let mut post_tags: Vec<post_tag::ActiveModel> = vec![];

        for post in posts
        {
            let number_of_tags = rng.gen_range(1..=5);
            let tags_to_use: Vec<&Model> = tags.choose_multiple(&mut rng, number_of_tags).collect();

            for tag in tags_to_use
            {
                post_tags.push(post_tag::ActiveModel {
                    post_id: Set(post.id),
                    tag_id: Set(tag.id),
                })
            }
        }

        post_tag::Entity::insert_many(post_tags).exec(db).await?;

        Ok(())
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr>
    {
        Ok(())
    }
}
