use sea_orm_migration::prelude::*;

use entity::{todo, user};

use crate::sea_orm::ActiveValue::Set;
use crate::sea_orm::EntityTrait;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        let db = manager.get_connection();

        let users = user::Entity::find().all(db).await?;

        let mut todos: Vec<todo::ActiveModel> = vec![];

        for user in users
        {
            for ctr in 1..=10
            {
                todos.push(todo::ActiveModel {
                    title: Set(format!("Todo {ctr}")),
                    descr: Set(Some(format!(
                        "This is todo number {ctr} by user {}",
                        user.id
                    ))),
                    user_id: Set(user.id),
                    is_done: Set(ctr > 5),
                    ..Default::default()
                });
            }
        }

        todo::Entity::insert_many(todos).exec(db).await?;

        Ok(())
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr>
    {
        Ok(())
    }
}
