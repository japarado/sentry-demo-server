use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .alter_table(
                Table::alter()
                    .table(Role::Table)
                    .add_column(ColumnDef::new(Role::Code).text().not_null())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .alter_table(
                Table::alter()
                    .table(Role::Table)
                    .drop_column(Role::Code)
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
enum Role
{
    Table,
    Code,
}
