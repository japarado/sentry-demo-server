use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .alter_table(
                Table::alter()
                    .table(Permission::Table)
                    .add_column(ColumnDef::new(Permission::Code).text().not_null())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .alter_table(
                Table::alter()
                    .table(Permission::Table)
                    .drop_column(Permission::Code)
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
enum Permission
{
    Table,
    Code,
}
