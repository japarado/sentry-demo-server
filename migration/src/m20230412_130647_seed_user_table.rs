use sea_orm_migration::prelude::*;

use entity::user;

use crate::sea_orm::ActiveValue::Set;
use crate::sea_orm::EntityTrait;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        let db = manager.get_connection();

        // TODO: Use with_capacity
        let mut active_models: Vec<user::ActiveModel> = vec![];

        for ctr in 1..=1000
        {
            active_models.push(user::ActiveModel {
                email: Set(format!("user{ctr}@mail.com")),
                password: Set("seeded password".to_owned()),
                salt: Set("something salt".to_string()),
                ..Default::default()
            });
        }

        user::Entity::insert_many(active_models).exec(db).await?;

        Ok(())
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr>
    {
        Ok(())
    }
}
