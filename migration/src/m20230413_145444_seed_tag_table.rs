use rand::distributions::{Alphanumeric, DistString};
use sea_orm_migration::prelude::*;

use entity::tag;

use crate::sea_orm::ActiveValue::Set;
use crate::sea_orm::EntityTrait;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        let db = manager.get_connection();

        let mut rng = rand::rngs::OsRng;

        let mut tags: Vec<tag::ActiveModel> = vec![];

        for _ in 1..=200
        {
            tags.push(tag::ActiveModel {
                name: Set(Alphanumeric.sample_string(&mut rng, 16)),
                descr: Set(Some(Alphanumeric.sample_string(&mut rng, 16))),
                ..Default::default()
            })
        }

        tag::Entity::insert_many(tags).exec(db).await?;

        Ok(())
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr>
    {
        Ok(())
    }
}
