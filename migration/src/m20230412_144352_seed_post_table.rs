use sea_orm_migration::prelude::*;

use entity::{post, user};

use crate::sea_orm::ActiveValue::Set;
use crate::sea_orm::EntityTrait;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        let db = manager.get_connection();

        let users = user::Entity::find().all(db).await?;

        // TODO: Use with_capacity
        let mut posts: Vec<post::ActiveModel> = vec![];

        for user in users
        {
            for ctr in 1..=10
            {
                posts.push(post::ActiveModel {
                    title: Set(format!("Post Number {ctr}")),
                    body: Set(format!("This is Post Number {ctr} by user {}", &user.email)),
                    user_id: Set(user.id),
                    ..Default::default()
                })
            }
        }

        post::Entity::insert_many(posts).exec(db).await?;

        Ok(())
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr>
    {
        Ok(())
    }
}
