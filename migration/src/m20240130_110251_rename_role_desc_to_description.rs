use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .alter_table(
                Table::alter()
                    .table(Role::Table)
                    .rename_column(Role::Desc, Role::Description)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .alter_table(
                Table::alter()
                    .table(Role::Table)
                    .rename_column(Role::Description, Role::Desc)
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
enum Role
{
    Table,
    Desc,
    Description,
}
