use sea_orm_migration::prelude::*;

use entity::{user, user_role};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .create_table(
                Table::create()
                    .table(UserRole::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(UserRole::UserId).integer().not_null())
                    .col(ColumnDef::new(UserRole::RoleId).integer().not_null())
                    .foreign_key(
                        ForeignKeyCreateStatement::new()
                            .from(user_role::Entity, user_role::Column::UserId)
                            .to(user::Entity, user::Column::Id),
                    )
                    .col(
                        ColumnDef::new(UserRole::CreatedAt)
                            .timestamp_with_time_zone()
                            .default(SimpleExpr::Keyword(Keyword::CurrentTimestamp))
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(UserRole::UpdatedAt)
                            .timestamp_with_time_zone()
                            .default(SimpleExpr::Keyword(Keyword::CurrentTimestamp))
                            .not_null(),
                    )
                    .col(ColumnDef::new(UserRole::DeletedAt).timestamp_with_time_zone())
                    .primary_key(
                        IndexCreateStatement::new()
                            .col(UserRole::UserId)
                            .col(UserRole::RoleId),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .drop_table(Table::drop().table(UserRole::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
enum UserRole
{
    Table,
    UserId,
    RoleId,
    CreatedAt,
    UpdatedAt,
    DeletedAt,
}
