pub use sea_orm_migration::prelude::*;

mod m20230100_174612_create_user_table;
mod m20230101_000001_create_post_table;
mod m20230412_041517_create_todo_table;
mod m20230412_130647_seed_user_table;
mod m20230412_131429_seed_todo_table;
mod m20230412_144352_seed_post_table;
mod m20230413_140226_create_tag_table;
mod m20230413_140249_create_post_tag_table;
mod m20230413_145444_seed_tag_table;
mod m20230413_150712_seed_post_tag_table;
mod m20240130_023353_create_role_table;
mod m20240130_023357_create_permission_table;
mod m20240130_023402_create_role_permission_table;
mod m20240130_055316_create_user_role_table;
mod m20240130_060356_add_timestamps_to_user_table;
mod m20240130_105446_rename_permission_desc_to_description;
mod m20240130_110251_rename_role_desc_to_description;
mod m20240130_110926_add_code_to_permission;
mod m20240130_112620_add_code_to_role;
mod m20240130_113338_seed_role_permissions;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator
{
    fn migrations() -> Vec<Box<dyn MigrationTrait>>
    {
        vec![
            Box::new(m20230100_174612_create_user_table::Migration),
            Box::new(m20230101_000001_create_post_table::Migration),
            Box::new(m20230412_041517_create_todo_table::Migration),
            Box::new(m20230412_130647_seed_user_table::Migration),
            Box::new(m20230412_131429_seed_todo_table::Migration),
            Box::new(m20230412_144352_seed_post_table::Migration),
            Box::new(m20230413_140226_create_tag_table::Migration),
            Box::new(m20230413_140249_create_post_tag_table::Migration),
            Box::new(m20230413_145444_seed_tag_table::Migration),
            Box::new(m20230413_150712_seed_post_tag_table::Migration),
            Box::new(m20240130_023353_create_role_table::Migration),
            Box::new(m20240130_023357_create_permission_table::Migration),
            Box::new(m20240130_023402_create_role_permission_table::Migration),
            Box::new(m20240130_055316_create_user_role_table::Migration),
            Box::new(m20240130_060356_add_timestamps_to_user_table::Migration),
            Box::new(m20240130_105446_rename_permission_desc_to_description::Migration),
            Box::new(m20240130_110251_rename_role_desc_to_description::Migration),
            Box::new(m20240130_110926_add_code_to_permission::Migration),
            Box::new(m20240130_112620_add_code_to_role::Migration),
            Box::new(m20240130_113338_seed_role_permissions::Migration),
        ]
    }
}
