use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .alter_table(
                Table::alter()
                    .table(Permission::Table)
                    .rename_column(Permission::Desc, Permission::Description)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .alter_table(
                Table::alter()
                    .table(Permission::Table)
                    .rename_column(Permission::Description, Permission::Desc)
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
enum Permission
{
    Table,
    Desc,
    Description,
}
