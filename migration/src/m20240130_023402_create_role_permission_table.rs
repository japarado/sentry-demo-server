use sea_orm_migration::prelude::*;

use entity::{role, role_permission};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .create_table(
                Table::create()
                    .table(RolePermission::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(RolePermission::RoleId).integer().not_null())
                    .col(
                        ColumnDef::new(RolePermission::PermissionId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKeyCreateStatement::new()
                            .from(role_permission::Entity, role_permission::Column::RoleId)
                            .to(role::Entity, role::Column::Id),
                    )
                    .foreign_key(
                        ForeignKeyCreateStatement::new()
                            .from(role_permission::Entity, role_permission::Column::RoleId)
                            .to(role::Entity, role::Column::Id),
                    )
                    .primary_key(
                        IndexCreateStatement::new()
                            .col(RolePermission::RoleId)
                            .col(RolePermission::PermissionId),
                    )
                    .col(
                        ColumnDef::new(RolePermission::CreatedAt)
                            .timestamp_with_time_zone()
                            .default(SimpleExpr::Keyword(Keyword::CurrentTimestamp))
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(RolePermission::UpdatedAt)
                            .timestamp_with_time_zone()
                            .default(SimpleExpr::Keyword(Keyword::CurrentTimestamp))
                            .not_null(),
                    )
                    .col(ColumnDef::new(RolePermission::DeletedAt).timestamp_with_time_zone())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        manager
            .drop_table(Table::drop().table(RolePermission::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
enum RolePermission
{
    Table,
    RoleId,
    PermissionId,
    CreatedAt,
    UpdatedAt,
    DeletedAt,
}
