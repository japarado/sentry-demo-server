use sea_orm_migration::prelude::*;

use entity::{permission, role, role_permission};

use crate::sea_orm::{EntityTrait, Set};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration
{
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        let db = manager.get_connection();

        let admin_role = role::ActiveModel {
            name: Set("Admin".to_string()),
            code: Set("admin".to_string()),
            ..Default::default()
        };

        let manager_role = role::ActiveModel {
            name: Set("Manager".to_string()),
            code: Set("manager".to_string()),
            ..Default::default()
        };

        let member_role = role::ActiveModel {
            name: Set("Member".to_string()),
            code: Set("member".to_string()),
            ..Default::default()
        };

        role::Entity::insert_many([admin_role, manager_role, member_role])
            .exec(db)
            .await?;

        let create_user_permission = permission::ActiveModel {
            name: Set("Create User".to_string()),
            code: Set("user:create".to_string()),
            description: Set(Some("Can create users".to_string())),
            ..Default::default()
        };
        let delete_user_permission = permission::ActiveModel {
            name: Set("Delete User".to_string()),
            code: Set("user:delete".to_string()),
            description: Set(Some("Can delete users".to_string())),
            ..Default::default()
        };
        let create_post_permission = permission::ActiveModel {
            name: Set("Create Post".to_string()),
            code: Set("post:create".to_string()),
            description: Set(Some("Can create posts".to_string())),
            ..Default::default()
        };
        let delete_post_permission = permission::ActiveModel {
            name: Set("Delete Post".to_string()),
            code: Set("post:delete".to_string()),
            description: Set(Some("Can delete posts".to_string())),
            ..Default::default()
        };

        permission::Entity::insert_many([
            create_user_permission,
            delete_user_permission,
            create_post_permission,
            delete_post_permission,
        ])
        .exec(db)
        .await?;

        role_permission::Entity::insert_many([
            // Admin
            role_permission::ActiveModel {
                role_id: Set(1),
                permission_id: Set(1),
                ..Default::default()
            },
            role_permission::ActiveModel {
                role_id: Set(1),
                permission_id: Set(2),
                ..Default::default()
            },
            role_permission::ActiveModel {
                role_id: Set(1),
                permission_id: Set(3),
                ..Default::default()
            },
            role_permission::ActiveModel {
                role_id: Set(1),
                permission_id: Set(4),
                ..Default::default()
            },
            // Manager
            role_permission::ActiveModel {
                role_id: Set(2),
                permission_id: Set(1),
                ..Default::default()
            },
            role_permission::ActiveModel {
                role_id: Set(2),
                permission_id: Set(3),
                ..Default::default()
            },
            role_permission::ActiveModel {
                role_id: Set(2),
                permission_id: Set(4),
                ..Default::default()
            },
            // Member
            role_permission::ActiveModel {
                role_id: Set(3),
                permission_id: Set(2),
                ..Default::default()
            },
            role_permission::ActiveModel {
                role_id: Set(3),
                permission_id: Set(4),
                ..Default::default()
            },
        ])
        .exec(db)
        .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr>
    {
        let db = manager.get_connection();
        role_permission::Entity::delete_many().exec(db).await?;
        role::Entity::delete_many().exec(db).await?;
        permission::Entity::delete_many().exec(db).await?;
        Ok(())
    }
}
