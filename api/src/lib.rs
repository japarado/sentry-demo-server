extern crate core;

use settings::Settings;

use crate::startup::Application;
use crate::telemetry::{get_subscriber, init_subscriber};

pub mod api;
pub mod startup;
pub mod telemetry;
mod utils;

#[tokio::main]
pub async fn main() -> anyhow::Result<()>
{
    let _guard = sentry::init(("https://2af78902ce384be4af38698db441b4c5@o4504223624134656.ingest.sentry.io/4504223625641984", sentry::ClientOptions {
        release: sentry::release_name!(),
        ..Default::default()
    }));

    // TODO Set if we're ever going to do a demo
    // std::env::set_var("RUST_BACKTRACE", "1");

    let configuration = Settings::assemble().expect("Failed to read configuration");

    let subscriber = get_subscriber(
        "sentry-demo-server",
        &configuration.logging.level,
        std::io::stdout,
    );
    init_subscriber(subscriber);

    let server = Application::build(configuration).await?;
    server.run_until_stopped().await?;

    Ok(())
}
