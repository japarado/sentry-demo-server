use std::fmt::{Debug, Display};

#[allow(unused)]
pub fn e500<T>(e: T) -> actix_web::Error
where
    T: Debug + Display + 'static,
{
    actix_web::error::ErrorInternalServerError(e)
}
