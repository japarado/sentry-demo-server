use actix_identity::Identity;
use actix_session::Session;
use actix_web::web::{Data, Json};
use actix_web::{HttpResponse, Responder};
use sea_orm::ActiveValue::Set;
use sea_orm::{ActiveModelTrait, DbConn};
use serde::Deserialize;

use entity::post;

#[derive(Deserialize)]
pub struct CreatePostPayload
{
    title: String,
    body: String,
}

pub async fn create_post(
    user: Identity,
    session: Session,
    db_conn: Data<DbConn>,
    payload: Json<CreatePostPayload>,
) -> impl Responder
{
    let CreatePostPayload { title, body } = payload.into_inner();
    let db_conn = db_conn.as_ref();
    let user_id = user.id().unwrap().parse::<i32>().unwrap();

    if let Some(count) = session.get::<i32>("counter").unwrap()
    {
        println!("COUNTER: {count}");
        println!("{:#?}", session.entries());
        session.insert("counter", count + 1).unwrap();
    }
    else
    {
        session.insert("counter", 1).unwrap();
    }

    let post = post::ActiveModel {
        title: Set(title),
        body: Set(body),
        user_id: Set(user_id),
        ..Default::default()
    }
    .insert(db_conn)
    .await;

    match post
    {
        Ok(post) => HttpResponse::Ok().json(post),
        Err(err) =>
        {
            HttpResponse::InternalServerError().json(format!("Failed save post. Error: {err}"))
        }
    }
}
