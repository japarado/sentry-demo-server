use argon2::password_hash::SaltString;
use argon2::Algorithm::Argon2id;
use argon2::{Argon2, Params, PasswordHash, PasswordHasher, PasswordVerifier, Version};
use secrecy::{ExposeSecret, Secret};

#[derive(thiserror::Error, Debug)]
pub enum AuthError
{
    #[error("Invalid credentials")]
    InvalidCredentials(#[source] anyhow::Error),
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

pub struct Credentials
{
    pub email: String,
    pub password: Secret<String>,
}

pub struct ComputedPasswordHash
{
    pub hash: Secret<String>,
    pub salt: SaltString,
}

impl ComputedPasswordHash
{
    pub fn new(hash: Secret<String>, salt: SaltString) -> Self
    {
        ComputedPasswordHash { hash, salt }
    }
}

// TODO: This method should return a Result
pub fn compute_password_hash(password: Secret<String>) -> ComputedPasswordHash
{
    let salt = SaltString::generate(&mut rand::thread_rng());

    // TODO: Better error handling
    let password_hash = Argon2::new(
        Argon2id,
        Version::V0x13,
        Params::new(15_000, 2, 1, None).unwrap(),
    )
    .hash_password(password.expose_secret().as_bytes(), &salt)
    .unwrap()
    .to_string();

    ComputedPasswordHash {
        hash: Secret::new(password_hash),
        salt,
    }
}

pub fn verify_password(password: Secret<String>, hash: &str) -> bool
{
    Argon2::default()
        .verify_password(
            password.expose_secret().as_bytes(),
            &PasswordHash::new(hash).unwrap(),
        )
        .is_ok()
}
