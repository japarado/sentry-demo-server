use actix_web::web::{delete, post, ServiceConfig};

use crate::api::auth_handlers::{login, logout, register};

pub mod auth_handlers;
pub mod auth_models;
pub mod password;

pub fn auth_routes(cfg: &mut ServiceConfig)
{
    cfg.route("register", post().to(register))
        .route("login", post().to(login))
        .route("logout", delete().to(logout));
}
