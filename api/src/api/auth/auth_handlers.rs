use actix_identity::Identity;
use actix_web::web::{Data, Form, Json};
use actix_web::{Either, HttpMessage, HttpRequest, HttpResponse, Responder};
use sea_orm::ActiveValue::Set;
use sea_orm::{ActiveModelTrait, ColumnTrait, DbConn, EntityTrait, NotSet, QueryFilter};
use secrecy::{ExposeSecret, Secret};
use tracing::instrument;

use entity::user;

use crate::api::auth_models::request::{LoginPayload, RegisterPayload};
use crate::api::password::{compute_password_hash, verify_password, ComputedPasswordHash};

// TODO: Move insert operation into DAO
pub async fn register(
    req: HttpRequest,
    db_conn: Data<DbConn>,
    payload: Either<Json<RegisterPayload>, Form<RegisterPayload>>,
) -> impl Responder
{
    let db_conn = db_conn.as_ref();
    let payload = payload.into_inner();

    let possible_current_user = user::Entity::find()
        .filter(user::Column::Email.eq(&payload.email))
        .one(db_conn)
        .await
        .unwrap();

    println!("{possible_current_user:#?}");

    if possible_current_user.is_some()
    {
        return HttpResponse::Conflict().json("User already has an account");
    }

    let ComputedPasswordHash { hash, salt } = compute_password_hash(Secret::new(payload.password));

    // TODO: Better error handling
    let user = user::ActiveModel {
        id: NotSet,
        email: Set(payload.email),
        password: Set(hash.expose_secret().to_string()),
        salt: Set(salt.to_string()),
    }
    .insert(db_conn)
    .await
    .unwrap();

    Identity::login(&req.extensions(), user.id.to_string()).unwrap();

    // TODO: Use a defined result type
    HttpResponse::Ok().json(user)
}

#[instrument(name = "Log in", fields(payload = ? payload), skip(db_conn, req))]
pub async fn login(
    req: HttpRequest,
    db_conn: Data<DbConn>,
    payload: Either<Json<LoginPayload>, Form<LoginPayload>>,
) -> impl Responder
{
    let db_conn = db_conn.as_ref();
    let payload = payload.into_inner();

    let user = user::Entity::find()
        .filter(user::Column::Email.eq(&payload.email))
        .one(db_conn)
        .await
        .unwrap();

    if let Some(user) = user
    {
        if verify_password(Secret::new(payload.password), &user.password)
        {
            Identity::login(&req.extensions(), user.id.to_string()).expect("Failed to log in");

            return HttpResponse::Ok().json(user);
        }
    }

    HttpResponse::Unauthorized().json("Invalid credentials")
}

#[instrument(name = "Log out", skip(user))]
pub async fn logout(user: Identity) -> impl Responder
{
    user.logout();
    HttpResponse::Ok().finish()
}
