pub mod request
{
    use serde::{Deserialize, Serialize};

    #[derive(Deserialize, Serialize, Debug)]
    pub struct RegisterPayload
    {
        pub email: String,
        pub password: String,
    }

    #[derive(Deserialize, Serialize, Debug)]
    pub struct LoginPayload
    {
        pub email: String,
        pub password: String,
    }
}

pub mod response
{
    use entity::user;
    use serde::Serialize;

    #[derive(Debug, Serialize)]
    pub struct LoginResponse
    {
        pub user: user::Model,
        pub jwt: String,
    }
}
