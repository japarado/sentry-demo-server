use actix_web::{HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use tracing::instrument;

pub use auth::*;
pub use post_create::*;
pub use post_search::*;

mod auth;
mod post_create;
mod post_search;

#[instrument(name = "Index route")]
pub async fn index() -> impl Responder
{
    HttpResponse::Ok().json("Sentry Demo Server root")
}

#[instrument(name = "API Index route")]
pub async fn api_index() -> impl Responder
{
    HttpResponse::Ok().json("Sentry Demo Server API root")
}

#[derive(Serialize, Deserialize)]
pub struct GenericNotFoundResponse
{
    pub message: String,
}

impl GenericNotFoundResponse
{
    pub fn new(message: String) -> Self
    {
        Self { message: message }
    }
}
