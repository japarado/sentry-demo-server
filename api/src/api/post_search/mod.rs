use actix_web::web::{Data, Query};
use actix_web::{HttpResponse, Responder};
use sea_orm::{ColumnTrait, Condition, DbConn, EntityTrait, QueryFilter};
use serde::Deserialize;

use entity::post;

#[derive(Deserialize)]
pub struct SearchPostsQuery
{
    q: Option<String>,
}

pub async fn search_posts(
    querystring: Query<SearchPostsQuery>,
    db_conn: Data<DbConn>,
) -> impl Responder
{
    let querystring = querystring.into_inner();
    let db_conn = db_conn.as_ref();

    let mut filter = Condition::any();

    if let Some(q) = querystring.q
    {
        filter = filter
            .add(post::Column::Title.like(format!("%{q}%")))
            .add(post::Column::Body.like(format!("%{q}%")))
    }

    let posts = post::Entity::find()
        .filter(filter)
        .all(db_conn)
        .await
        .unwrap();

    HttpResponse::Ok().json(posts)
}
