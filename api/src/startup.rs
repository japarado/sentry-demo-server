use std::io;
use std::net::TcpListener;

use actix_cors::Cors;
use actix_identity::IdentityMiddleware;
use actix_session::storage::RedisSessionStore;
use actix_session::SessionMiddleware;
use actix_web::cookie::Key;
use actix_web::dev::Server;
use actix_web::middleware::TrailingSlash;
use actix_web::web::Data;
use actix_web::{middleware, web, App, HttpServer};
use sea_orm::{ConnectOptions, Database, DatabaseConnection};
use tracing_actix_web::TracingLogger;

use settings::{DatabaseSettings, Settings};

use crate::api::{api_index, auth_routes, create_post, index, search_posts};

pub struct Application
{
    port: u16,
    server: Server,
}

impl Application
{
    pub async fn build(configuration: Settings) -> Result<Self, anyhow::Error>
    {
        let db_conn = get_db_conn(&configuration.database).await;

        let address = format!(
            "{}:{}",
            configuration.application.host, configuration.application.port
        );

        let listener = TcpListener::bind(address)?;
        let port = listener.local_addr().unwrap().port();
        let server = run(listener, db_conn).await?;

        Ok(Self { server, port })
    }

    pub fn port(&self) -> u16
    {
        self.port
    }

    pub async fn run_until_stopped(self) -> Result<(), io::Error>
    {
        self.server.await
    }
}

pub async fn get_db_conn(config: &DatabaseSettings) -> DatabaseConnection
{
    let mut opt = ConnectOptions::new(config.to_string());
    opt.max_connections(config.max_connections)
        .min_connections(config.min_connections);
    Database::connect(opt)
        .await
        .expect("Failed to establish a DB connection")
}

async fn run(listener: TcpListener, db_conn: DatabaseConnection) -> Result<Server, anyhow::Error>
{
    let db_conn = Data::new(db_conn);

    let session_secret_key = Key::generate();
    let redis_store = RedisSessionStore::new("redis://127.0.0.1:6379")
        .await
        .expect("Failed to connect to Redis");

    let server = HttpServer::new(move || {
        App::new()
            .wrap(IdentityMiddleware::default())
            .wrap(SessionMiddleware::new(
                redis_store.clone(),
                session_secret_key.clone(),
            ))
            .wrap(TracingLogger::default())
            .wrap(sentry_actix::Sentry::new())
            .wrap(middleware::NormalizePath::new(TrailingSlash::Trim))
            .wrap(Cors::permissive())
            .route("/", web::get().to(index))
            .service(
                web::scope("/api")
                    .route("/posts", web::post().to(create_post))
                    .route("/posts", web::get().to(search_posts))
                    .route("", web::get().to(api_index))
                    .service(web::scope("/auth").configure(auth_routes)),
            )
            .app_data(db_conn.clone())
    })
    .listen(listener)?
    .run();
    Ok(server)
}
