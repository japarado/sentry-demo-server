FROM rust:latest as builder

WORKDIR /app

COPY . .

RUN cargo install sea-orm-cli

RUN sea migrate

RUN cargo build --workspace

WORKDIR /app

FROM archlinux:base-devel

COPY --from=builder /app/target/release/sentry-demo-server .

EXPOSE 8080

CMD ["./sentry-demo-server"]
