use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, DeriveEntityModel, Deserialize, Serialize)]
#[sea_orm(table_name = "user")]
pub struct Model
{
    #[sea_orm(primary_key)]
    #[serde(skip_deserializing)]
    pub id: i32,
    pub email: String,
    #[serde(skip_serializing)]
    pub password: String,
    #[serde(skip_serializing)]
    pub salt: String,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation
{
    #[sea_orm(has_many = "super::post::Entity")]
    Post,
    #[sea_orm(has_many = "super::todo::Entity")]
    Todo,
}

impl Related<super::post::Entity> for Entity
{
    fn to() -> RelationDef
    {
        Relation::Post.def()
    }
}

impl Related<super::todo::Entity> for Entity
{
    fn to() -> RelationDef
    {
        Relation::Todo.def()
    }
}

impl ActiveModelBehavior for ActiveModel
{
}
